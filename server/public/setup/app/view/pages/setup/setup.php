<div class="container mt-5">

    <h1 class="h4 mb-3 font-weight-normal text-center"><?= $h1 ?></h1>

    <div class="row">
        <div class="col-md-2 text-center mt-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="#17a2b8" class="bi bi-tools" viewBox="0 0 16 16">
                <path d="M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.356 3.356a1 1 0 0 0 1.414 0l1.586-1.586a1 1 0 0 0 0-1.414l-3.356-3.356a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0zm9.646 10.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708zM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11z"/>
            </svg>
        </div>
        <div class="col-md-8 mt-3">

            <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">

                <div class="form-group">
                    <label for="hostInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Host (ex.: wpbit_mysql)
                    </label>
                    <input type="text" name="host" class="form-control" id="hostInput" required>
                </div>

                <div class="form-group">
                    <label for="portInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Port (ex.: 3306)
                    </label>
                    <input type="text" name="port" class="form-control" id="portInput" required>
                </div>

                <div class="form-group">
                    <label for="dbNameInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Database Name (ex.: wpbit_mysql)
                    </label>
                    <input type="text" name="dbname" class="form-control" id="dbNameInput" required>
                </div>

                <div class="form-group">
                    <label for="userNameInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Username (ex.: root)
                    </label>
                    <input type="text" name="uname" class="form-control" id="userNameInput" required>
                </div>

                <div class="form-group">
                    <label for="passwordInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Password (ex.: password)
                    </label>
                    <input type="password" name="pass" class="form-control" id="passwordInput" required>
                </div>

                <input type="hidden" name="token" value="">

                <div class="text-center">
                    <button type="submit" class="btn btn-info mt-4">Сохранить</button>
                </div>

            </form>

        </div>
        <div class="col-md-2"></div>
    </div>

</div>
