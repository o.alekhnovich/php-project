<?php

namespace setup\app\controller;
use setup\View;

class Setup
{
    public $host, $port, $dbname, $charset, $uname, $pass;

    /**
     *
     */
    public function setup()
    {
        if (isset($_POST['host']) && $_POST['host'] != '' &&
            isset($_POST['port']) && $_POST['port'] != '' &&
            isset($_POST['dbname']) && $_POST['dbname'] != '' &&
            isset($_POST['uname']) && $_POST['uname'] != '' &&
            isset($_POST['pass']) && $_POST['pass'] != '') {

            $this->host = "define('DB_HOST', '" . htmlspecialchars(trim($_POST['host'])) . "');";
            $this->port = "define('DB_PORT', '" . htmlspecialchars(trim($_POST['port'])) . "');";
            $this->dbname = "define('DB_NAME', '" . htmlspecialchars(trim($_POST['dbname'])) . "');";
            $this->charset = "define('DB_CHARSET', 'UTF8');";
            $this->uname = "define('DB_USER', '" . htmlspecialchars(trim($_POST['uname'])) . "');";
            $this->pass = "define('DB_PASS', '" . htmlspecialchars(trim($_POST['pass'])) . "');";

            // Check connection to DB
            try {
                $pdo = new \PDO (
                    'mysql:host=' . htmlspecialchars(trim($_POST['host']))
                    . ';port=' . htmlspecialchars(trim($_POST['port']))
                    . ';charset=UTF8'
                    . ';dbname=' . htmlspecialchars(trim($_POST['dbname'])),
                    htmlspecialchars(trim($_POST['uname'])),
                    htmlspecialchars(trim($_POST['pass'])));

                $this->createConfigFile();

            } catch (\PDOException $e) {
                $_SESSION['alerts'] = ['err' => $e->getMessage()];
                //echo "Error!: " . $e->getMessage();
            }

        }

        $title = 'Настройки БД';
        $h1 = 'Давайте настроем БД';

        $this->content_data = compact('title', 'h1');

        $this->getView(__CLASS__, __FUNCTION__);
    }

    /**
     *
     */
    protected function createConfigFile()
    {
        $config_file = fopen('../config.php', 'w');
        $file_content = "<?php" . "\n" . $this->host . "\n" . $this->port . "\n" . $this->dbname . "\n" . $this->charset . "\n" . $this->uname . "\n" . $this->pass;

        fwrite($config_file, $file_content);
        fclose($config_file);

        $_SESSION['alerts'] = ['success' => 'Настройки сохранены'];
        header('Location: /register');
    }

    /**
     * @param $class_name
     * @param $page_action
     * @return bool
     */
    protected function getView($class_name, $page_action)
    {
        $page_name = self::getPageName($class_name);

        $templateView = new View($page_name, $page_action);
        $templateView->templateView($this->content_data);

        return true;
    }

    /**
     * @param $class_name
     * @return string
     */
    protected static function getPageName($class_name)
    {
        $class_name =  explode('\\', $class_name);
        $class_name = $class_name[count($class_name) - 1];

        return lcfirst($class_name);
    }

}
