<?php

session_start();

if (!empty($_SESSION['alerts'])) {
    unset($_SESSION['alerts']);
}

if (isset($_SESSION['LoggedIn'])) {
    unset($_SESSION['LoggedIn']);
}

use setup\Router;

function allClasses($class) {
    $file = '../' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
}
spl_autoload_register('allClasses');


// URI
$uri = $_SERVER['REQUEST_URI'];


// Routes
$routes['setup'] = ['controller' => 'setup', 'action' => 'setup'];


// Get Route
Router::getRoute($uri, $routes);
