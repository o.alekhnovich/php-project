<?php

namespace setup;

class View
{
    public $page_content_file;

    public function __construct($page_name, $page_action)
    {
        $this->page_content_file = '../setup/app/view/pages/' . $page_name . '/' . $page_action . '.php';
    }

    /**
     * @param $page_data
     */
    public function templateView($page_data)
    {
        extract($page_data);

        // Header
        include '../setup/app/view/layout/header.php';

        // Alerts
        if (!empty($_SESSION['alerts'])) {
            include '../setup/app/view/layout/alerts.php';
        }

        // Main Content
        if (file_exists($this->page_content_file)) {
            include $this->page_content_file;
        } else {
            echo 'view file not found...';
        }

        // Footer
        include '../setup/app/view/layout/footer.php';

    }
}
