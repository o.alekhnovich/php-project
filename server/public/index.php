<?php

session_start();

if (!file_exists('config.php')) {
    header('Location: setup/');
}

use core\Router;

function allClasses($class) {
    $file = '../' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
}
spl_autoload_register('allClasses');

// Main Config
require_once 'config.php';
define('DIR_FILE_UPLOAD', 'upload/');
define('LOGIN_PAGE', 'login');
define('REG_PAGE', 'register');
define('START_PAGE', '/');

// URI
$uri = $_SERVER['REQUEST_URI'];

// Routes
$auth_query = ['login', 'register', 'newuser', 'signin'];
$routes['login'] = ['controller' => 'login', 'action' => 'login'];
$routes['register'] = ['controller' => 'login', 'action' => 'register'];
$routes['newuser'] = ['controller' => 'login', 'action' => 'newuser'];
$routes['signin'] = ['controller' => 'login', 'action' => 'signin'];
$routes['logout'] = ['controller' => 'login', 'action' => 'logout'];

$routes[''] = ['controller' => 'account', 'action' => 'dashboard'];
$routes['add'] = ['controller' => 'account', 'action' => 'add'];
$routes['settings'] = ['controller' => 'account', 'action' => 'settings'];
$routes['uploadfile'] = ['controller' => 'upload', 'action' => 'save'];


// Get Route
Router::getRoute($uri, $routes, $auth_query);
