<?php

namespace app\model;

use core\Database;
use core\File;

class Member
{
    public $pdo;
    public $fileTxt;
    public $fileCsv;

    public function __construct()
    {
        $this->pdo = Database::getInstance();
    }

    /**
     * @param $userid
     * @return array|false
     */
    public function getAllMembers($userid)
    {
        $sql = "SELECT * FROM members WHERE userid = $userid";
        return $this->pdo->getDbResult($sql);
    }

    /**
     * @param $userid
     * @return array
     */
    public function getAllMembersTxt($userid)
    {
        $filename = $this->getFileName($userid, 'filetxt');

        if ($filename) {
            $this->fileTxt = File::getInstanceTxt($filename);
        } else {
            $this->fileTxt = null;
        }

        if ($this->fileTxt) {
            while (($buffer = fgets($this->fileTxt)) !== false) {
                $row = explode('|*|', $buffer);
                $txtRecords[] = [
                    'nickname' => $row[0],
                    'email' => $row[1],
                ];
            }
            if (!empty($txtRecords)) {
                return $txtRecords;
            }
        }
        return [];
    }

    /**
     * @param $userid
     * @return array
     */
    public function getAllMembersCsv($userid)
    {
        $filename = $this->getFileName($userid, 'filecsv');

        if ($filename) {
            $this->fileCsv = File::getInstanceCsv($filename);
        } else {
            $this->fileCsv = null;
        }

        if ($this->fileCsv) {
            for ($i = 0; $data = fgetcsv($this->fileCsv, 1000, ","); $i++)
            {
                $csvRecords[] = [
                    'nickname' => $data[0],
                    'email' => $data[1],
                ];
            }
            if (!empty($csvRecords)) {
                return $csvRecords;
            }
        }
        return [];
    }

    /**
     * @param $nickname
     * @param $email
     * @param $phone
     * @param $userid
     * @return bool
     */
    public function saveToDb($nickname, $email, $phone, $userid)
    {
        $sql = "INSERT INTO members(nickname, email, phone, userid) VALUES ('$nickname', '$email', '$phone', '$userid')";

        $this->pdo->getDbResult($sql);

        return true;
    }

    /**
     * @param $nickname
     * @param $email
     * @param $phone
     * @param $userid
     * @return bool
     */
    public function saveToTxt($nickname, $email, $phone, $userid)
    {
        $filename = $this->getFileName($userid, 'filetxt');

        if ($filename) {
            $this->fileTxt = File::getInstanceTxt($filename);
        } else {
            $this->fileTxt = null;
        }

        $new_member = $nickname . '|*|' . $email . '|*|' . $phone . '|*|' . $userid . "\n";

        if ($this->fileTxt) {
            fwrite($this->fileTxt, $new_member);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $nickname
     * @param $email
     * @param $phone
     * @param $userid
     * @return bool
     */
    public function saveToCsv($nickname, $email, $phone, $userid)
    {
        $filename = $this->getFileName($userid, 'filecsv');

        if ($filename) {
            $this->fileCsv = File::getInstanceCsv($filename);
        } else {
            $this->fileCsv = null;
        }

        $new_member[] = [
            'nickname' => $nickname,
            'email' => $email,
            'phone' => $phone,
            'userid' => $userid,
        ];

        if ($this->fileCsv) {
            foreach ($new_member as $row => $col) {
                fputcsv($this->fileCsv, $col);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $userid
     * @param $table_column
     * @return mixed|string|void
     */
    public function getFileName($userid, $table_column)
    {
        $sql = "SELECT $table_column FROM users WHERE id = $userid";

        $res = $this->pdo->getDbResult($sql);

        if (!empty($res)) {
            foreach ($res as $row) {
                return $row[$table_column];
            }
        } else {
            return 'null';
        }
    }

}
