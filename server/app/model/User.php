<?php

namespace app\model;

use core\Database;

class User
{
    public $pdo;

    public function __construct()
    {
        $this->pdo = Database::getInstance();
    }

    /**
     * @param $email
     * @param $pass
     * @return mixed|null
     */
    public function checkUser($email, $pass)
    {
        $sql = "SELECT id, email, password FROM users WHERE email = '$email'";

        $res = $this->pdo->getDbResult($sql);

        if (!empty($res)) {
            foreach ($res as $row) {
                if (password_verify($pass, $row['password'])) {
                    return ($row['id']);
                }
            }
        }

        return null;
    }

    /**
     * @param $email
     * @param $pass
     * @return mixed|null
     */
    public function createUser($email, $pass)
    {
        $sql = "SELECT email FROM users WHERE email = '$email'";

        if (empty($this->pdo->getDbResult($sql))) {

            $pass = password_hash($pass, PASSWORD_DEFAULT);

            $sql = "INSERT INTO users(email, password, nickname) VALUES ('$email', '$pass', '')";
            $this->pdo->getDbResult($sql);

            $sql = "SELECT id FROM users WHERE email = '$email'";
            $res = $this->pdo->getDbResult($sql);

            if (!empty($res)) {
                foreach ($res as $row) {
                    return ($row['id']);
                }
            }
        }
        return null;
    }
}
