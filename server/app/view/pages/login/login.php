<div class="container mt-5">

    <form class="form-signin" action="signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal text-center"><?= $h1 ?></h1>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required>
        <label for="inputPassword" class="sr-only">Пароль</label>
        <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Пароль" required>

        <input type="hidden" name="token" value="<?= $token ?>">

        <button class="btn btn-lg btn-info btn-block" type="submit">Войти</button>
    </form>

    <p class="text-center">-- или --</p>

    <p class="text-center">
        <a href="register">Зарегистрируйтесь</a>
    </p>

</div>
