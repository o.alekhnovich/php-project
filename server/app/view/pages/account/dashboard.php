<div class="container mt-5">

    <h1 class="h4 text-center"><?= $h1 ?></h1>

    <div class="row mt-5 pt-5">

        <div class="col-md-4">
            <div class="border">
                <div class="d-flex justify-content-center">
                    <div class="rounded-circle d-flex justify-content-center align-items-center mt-n5" style="width: 96px; height: 96px; background-color: whitesmoke;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" fill="currentColor" class="bi bi-file-earmark-text" viewBox="0 0 16 16">
                            <path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                            <path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                        </svg>&nbsp;TXT
                    </div>
                </div>
                <ul class="list-group list-group-flush mt-4">
                    <?php foreach ($txtRecords as $row): ?>
                        <li class="list-group-item"><?= $row['nickname'] . ' (' . $row['email'] . ')' ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="col-md-4">
            <div class="border">
                <div class="d-flex justify-content-center">
                    <div class="rounded-circle d-flex justify-content-center align-items-center mt-n5" style="width: 96px; height: 96px; background-color: whitesmoke;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" fill="currentColor" class="bi bi-file-earmark-spreadsheet" viewBox="0 0 16 16">
                            <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V9H3V2a1 1 0 0 1 1-1h5.5v2zM3 12v-2h2v2H3zm0 1h2v2H4a1 1 0 0 1-1-1v-1zm3 2v-2h3v2H6zm4 0v-2h3v1a1 1 0 0 1-1 1h-2zm3-3h-3v-2h3v2zm-7 0v-2h3v2H6z"/>
                        </svg>&nbsp;CSV
                    </div>
                </div>
                <ul class="list-group list-group-flush mt-4">
                    <?php foreach ($csvRecords as $row): ?>
                        <li class="list-group-item"><?= $row['nickname'] . ' (' . $row['email'] . ')' ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="col-md-4">
            <div class="border">
                <div class="d-flex justify-content-center">
                    <div class="rounded-circle d-flex justify-content-center align-items-center mt-n5" style="width: 96px; height: 96px; background-color: whitesmoke;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" fill="currentColor" class="bi bi-hdd-stack" viewBox="0 0 16 16">
                            <path d="M14 10a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1h12zM2 9a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-1a2 2 0 0 0-2-2H2z"/>
                            <path d="M5 11.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm-2 0a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zM14 3a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h12zM2 2a2 2 0 0 0-2 2v1a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H2z"/>
                            <path d="M5 4.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm-2 0a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
                        </svg>&nbsp;DB
                    </div>
                </div>
                <ul class="list-group list-group-flush mt-4">
                    <?php foreach ($dbRecords as $row): ?>
                    <li class="list-group-item"><?= $row['nickname'] . ' (' . $row['email'] . ')' ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

    </div>

</div>
