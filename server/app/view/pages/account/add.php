<div class="container mt-5">

    <h1 class="h4 mb-3 font-weight-normal text-center"><?= $h1 ?></h1>

    <div class="row">
        <div class="col-md-2 text-center mt-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="#17a2b8" class="bi bi-person-circle" viewBox="0 0 16 16">
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
            </svg>
        </div>
        <div class="col-md-8 mt-3">
            <form action="add" method="post">
                <div class="form-group">
                    <label for="nameInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Ваш Ник
                    </label>
                    <input type="text" name="nickname" class="form-control" id="nameInput" required>
                </div>
                <div class="form-group">
                    <label for="emailInput" class="small text-muted">
                        <span class="text-danger">*</span>
                        Адрес электронной почты
                    </label>
                    <input type="email" name="email" class="form-control" id="emailInput" required>
                </div>
                <div class="form-group">
                    <label for="phoneInput" class="small text-muted">
                        Номер телефона (опционально)
                    </label>
                    <input type="tel" name="phone" class="form-control" id="phoneInput">
                </div>

                <div class="form-check mt-4">
                    <input class="form-check-input" type="radio" name="storage" id="saveToTxt" value="txt">
                    <label class="form-check-label" for="saveToTxt">
                        Сохранить в файл TXT
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="storage" id="saveToCsv" value="csv">
                    <label class="form-check-label" for="saveToCsv">
                        Сохранить в файл CSV
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="storage" id="saveToDb" value="db" checked>
                    <label class="form-check-label" for="saveToDb">
                        Сохранить в Базу Данных
                    </label>
                </div>

                <input type="hidden" name="token" value="<?= $token ?>">

                <div class="text-center">
                    <button type="submit" class="btn btn-info mt-4">Добавить</button>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>

</div>
