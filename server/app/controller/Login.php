<?php

namespace app\controller;
use core\Controller;
use  app\model\User;

class Login extends Controller
{
    public $content_data = [];

    public function login()
    {
        $title = 'Вход в личный кабинет';
        $h1 = 'Авторизуйтесь';
        $token = $this->getToken();
        $this->content_data = compact('title', 'h1', 'token');

        $this->getView(__CLASS__, __FUNCTION__);
    }

    public function register()
    {
        $title = 'Новый пользователь';
        $h1 = 'Зарегистрируйтесь';
        $token = $this->getToken();
        $this->content_data = compact('title', 'h1', 'token');

        $this->getView(__CLASS__, __FUNCTION__);
    }

    public function logout()
    {
        unset($_SESSION['LoggedIn']);
        unset($_SESSION['userid']);
        unset($_SESSION['token']);
        $redirect = 'Location: ' . LOGIN_PAGE;
        header($redirect);
    }

    public function signin()
    {
        if (isset($_POST['email']) && $_POST['email'] != '' &&
            isset($_POST['pass']) && $_POST['pass'] != '' &&
            isset($_POST['token']) && $_POST['token'] != '') {

            $email = htmlspecialchars($_POST['email']);
            $pass = htmlspecialchars($_POST['pass']);
            $token = htmlspecialchars($_POST['token']);

            if ($this->checkToken($token)) {

                unset($_SESSION['token']);
                $_SESSION['token'] = $this->getToken();

                if ($this->checkEmail($email)) {

                    $exist_user = new User();

                    if ($userid = $exist_user->checkUser($email, $pass)) {

                        $_SESSION['LoggedIn'] = true;
                        $_SESSION['userid'] = $userid;
                        $redirect = 'Location: ' . START_PAGE;
                        header($redirect);

                    } else {
                        $_SESSION['alerts'] = ['err' => 'Неверный логин или пароль'];
                        unset($_SESSION['token']);
                        $redirect = 'Location: ' . LOGIN_PAGE;
                        header($redirect);
                    }
                }
            } else {
                $redirect = 'Location: ' . LOGIN_PAGE;
                unset($_SESSION['token']);
                header($redirect);
            }
        }
    }

    public function newuser()
    {
        if (isset($_POST['email']) && $_POST['email'] != '' &&
            isset($_POST['pass']) && $_POST['pass'] != '' &&
            isset($_POST['repeatpass']) && $_POST['repeatpass'] != '') {

            $email = htmlspecialchars($_POST['email']);
            $pass = htmlspecialchars($_POST['pass']);
            $repeatpass = htmlspecialchars($_POST['repeatpass']);

            if ($this->checkEmail($email)) {

                if ($pass == $repeatpass) {

                    $new_user = new User();
                    $userid = $new_user->createUser($email, $pass);

                    if (!empty($userid)) {

                        mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . DIR_FILE_UPLOAD . $userid, 0700);

                        $_SESSION['LoggedIn'] = true;
                        $_SESSION['userid'] = $userid;
                        $_SESSION['alerts'] = ['success' => 'Вы успешно зарегистрировались!'];
                        $redirect = 'Location: ' . START_PAGE;
                        header($redirect);

                    } else {
                        $_SESSION['alerts'] = ['err' => 'Такой пользователь уже зарегистрирован'];
                        $redirect = 'Location: ' . REG_PAGE;
                        header($redirect);
                    }
                } else {
                    $_SESSION['alerts'] = ['err' => 'Введенные пароли не совпадают'];
                    $redirect = 'Location: ' . REG_PAGE;
                    header($redirect);
                }
            } else {
                $_SESSION['alerts'] = ['err' => 'Введен некорректный Email'];
                $redirect = 'Location: ' . REG_PAGE;
                header($redirect);
            }
        } else {
            $_SESSION['alerts'] = ['err' => 'Одно из полей не заполнено'];
            $redirect = 'Location: ' . REG_PAGE;
            header($redirect);
        }
    }

}
