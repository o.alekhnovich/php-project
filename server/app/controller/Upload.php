<?php

namespace app\controller;
use core\Controller;
use app\model\Settings;

class Upload extends Controller
{
    public $content_data = [];

    public function save()
    {
        if (isset($_FILES['userfile'])) {

            if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {

                $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/' . DIR_FILE_UPLOAD . $_SESSION['userid'] . '/';

                $filename = basename($_FILES['userfile']['name']);

                if (!file_exists(DIR_FILE_UPLOAD . $_SESSION['userid'] . '/' . $filename)) {

                    $filetype = $_FILES['userfile']['type'];

                    $arr = explode('.', $filename);
                    $file_extension = strtolower(array_pop($arr));

                    if ($this->checkFile($file_extension, $filetype)) {

                        move_uploaded_file($_FILES['userfile']['tmp_name'], $uploaddir . $filename);

                        $setfile = new Settings();
                        $setfile->setFile($filename, 'file' . $file_extension, $_SESSION['userid']);

                        $_SESSION['alerts'] = ['success' => 'Файл успешно загружен'];

                    } else {
                        $_SESSION['alerts'] = ['err' => 'Вы пытаетесь загрузить файл с другим расширением'];
                    }
                } else {
                    $_SESSION['alerts'] = ['err' => 'Такой файл уже существует'];
                }
            }
        }
        $redirect = 'Location: settings';
        header($redirect);
    }
}
