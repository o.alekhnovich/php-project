<?php

namespace app\controller;
use core\Controller;
use  app\model\Member;

class Account extends Controller
{
    public $content_data = [];

    public function dashboard()
    {
        $title = 'Главная панель';
        $h1 = 'Список всех контактов';
        $members = new Member();
        $dbRecords = $members->getAllMembers($_SESSION['userid']);
        $txtRecords = $members->getAllMembersTxt($_SESSION['userid']);
        $csvRecords = $members->getAllMembersCsv($_SESSION['userid']);

        $this->content_data = compact('title', 'h1', 'dbRecords', 'txtRecords', 'csvRecords');

        $this->getView(__CLASS__, __FUNCTION__);
    }

    public function add()
    {
        if (isset($_POST['nickname']) && $_POST['nickname'] != '' &&
            isset($_POST['email']) && $_POST['email'] != '' &&
            isset($_POST['storage']) && $_POST['storage'] != '' &&
            isset($_POST['token']) && $_POST['token'] != '') {

            if (isset($_POST['phone']) && $_POST['phone'] != '') {
                $phone = htmlspecialchars($_POST['phone']);
            } else {
                $phone = '';
            }

            $nickname = htmlspecialchars($_POST['nickname']);
            $email = htmlspecialchars($_POST['email']);
            $token = htmlspecialchars($_POST['token']);
            $storage = $_POST['storage'];

            if ($this->checkToken($token)) {

                unset($_SESSION['token']);
                $_SESSION['token'] = $this->getToken();

                $new_member = new Member();

                switch ($storage) {
                    case 'db':
                        $new_member->saveToDb($nickname, $email, $phone, $_SESSION['userid']);
                        $_SESSION['alerts'] = ['success' => 'Вы успешно добавили контакт!'];
                        break;
                    case 'txt':
                        if ($new_member->saveToTxt($nickname, $email, $phone, $_SESSION['userid'])) {
                            $_SESSION['alerts'] = ['success' => 'Вы успешно добавили контакт!'];
                        } else {
                            $_SESSION['alerts'] = ['err' => 'Ошибка при добавлении контакта'];
                        }
                        break;
                    case 'csv':
                        if ($new_member->saveToCsv($nickname, $email, $phone, $_SESSION['userid'])) {
                            $_SESSION['alerts'] = ['success' => 'Вы успешно добавили контакт!'];
                        } else {
                            $_SESSION['alerts'] = ['err' => 'Ошибка при добавлении контакта'];
                        }
                        break;
                    default:
                        break;
                }
            } else {
                unset($_SESSION['token']);
                $_SESSION['alerts'] = ['err' => 'Неверный токен'];
            }
        }

        $title = 'Новый контакт';
        $h1 = 'Добавление нового контакта';
        $token = $this->getToken();
        $this->content_data = compact('title', 'h1', 'token');

        $this->getView(__CLASS__, __FUNCTION__);
    }

    public function settings()
    {
        $title = 'Настройки';
        $h1 = 'Основные настроки';
        $fileList = array_diff(scandir(DIR_FILE_UPLOAD . $_SESSION['userid'], 1), array('.', '..'));

        if (empty($fileList)) {
            $fileList = ['Нет загруженных файлов'];
        }

        $this->content_data = compact('title', 'h1', 'fileList');

        $this->getView(__CLASS__, __FUNCTION__);
    }

}
