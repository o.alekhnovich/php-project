<?php

namespace core;

class View
{
    public $page_content_file;

    /**
     * @param $page_name
     * @param $page_action
     */
    public function __construct($page_name, $page_action)
    {
        $this->page_content_file = '../app/view/pages/' . $page_name . '/' . $page_action . '.php';
    }

    /**
     * @param $page_data
     */
    public function templateView($page_data)
    {
        extract($page_data);

        // Header
        include '../app/view/layout/header.php';

        // Navbar
        if (isset($_SESSION['LoggedIn'])) {
            include '../app/view/layout/navbar-menu.php';
        } else {
            include '../app/view/layout/navbar.php';
        }

        // Alerts
        if (!empty($_SESSION['alerts'])) {
            include '../app/view/layout/alerts.php';
        }

        // Main Content
        if (file_exists($this->page_content_file)) {
            include $this->page_content_file;
        } else {
            echo 'view file not found...';
        }

        // Footer
        include '../app/view/layout/footer.php';

        // Clean alerts
        if (!empty($_SESSION['alerts'])) {
            unset($_SESSION['alerts']);
        }

    }
}
