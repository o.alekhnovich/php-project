<?php

namespace core;

class File
{
    protected $file;
    protected static $filename;
    protected static $instanceTxt;
    protected static $instanceCsv;

    private function __clone() {}
    private function __wakeup() {}

    protected function __construct()
    {
        $file = DIR_FILE_UPLOAD . $_SESSION['userid'] . '/' . self::$filename;

        if (file_exists($file)) {
            $this->file = fopen($file, 'a+');
        } else {
            $this->file = null;
        }
    }

    /**
     * @param $filename
     * @return false|resource|null
     */
    public static function getInstanceTxt($filename)
    {
        if (!isset(self::$instanceTxt)) {
            self::$filename = $filename;
            self::$instanceTxt = new self;
        }

        return self::$instanceTxt->file;
    }

    /**
     * @param $filename
     * @return false|resource|null
     */
    public static function getInstanceCsv($filename)
    {
        if (!isset(self::$instanceCsv)) {
            self::$filename = $filename;
            self::$instanceCsv = new self;
        }

        return self::$instanceCsv->file;
    }

}
