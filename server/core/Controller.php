<?php

namespace core;

abstract class Controller
{
    /**
     * @param $class_name
     * @param $page_action
     * @return bool
     */
    public function getView($class_name, $page_action)
    {
        $page_name = Controller::getPageName($class_name);

        $templateView = new View($page_name, $page_action);
        $templateView->templateView($this->content_data);

        return true;
    }

    /**
     * @param $class_name
     * @return string
     */
    protected static function getPageName($class_name)
    {
        $class_name =  explode('\\', $class_name);
        $class_name = $class_name[count($class_name) - 1];

        return lcfirst($class_name);
    }

    /**
     * @param $email
     * @return bool
     */
    public function checkEmail($email)
    {
        return true;
    }

    /**
     * @param $file_extension
     * @param $filetype
     * @return bool
     */
    public function checkFile($file_extension, $filetype)
    {
        $valid_types = [
            'txt' => 'text/plain',
            'csv' => 'text/csv',
        ];

        if (array_key_exists($file_extension, $valid_types) && $valid_types[$file_extension] == $filetype) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return false|mixed|string
     */
    public  function  getToken()
    {
        if (isset($_SESSION['token']) && $_SESSION['token'] != '') {
            return $_SESSION['token'];
        } else {
            $_SESSION['token'] = hash('sha512', uniqid());
            return $_SESSION['token'];
        }
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkToken($token)
    {
        if (isset($_SESSION['token']) && $token == $_SESSION['token']) {
            return true;
        } else {
            return false;
        }
    }

}
